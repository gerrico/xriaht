
;xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같      Progetto :   serie P R I M E    A W 1 6           xxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같   xxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx컓xxxxxxxxx같
;xxxxxxx같
;xxxxxxx같      Data Creazione :        13 - 09 - 2006
;xxxxxxx같
;xxxxxxx같   xxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx컓xxxxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같      Revisione:                                        xxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같   xxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx컓xxxxxxxxx같
;xxxxxxx같
;xxxxxxx같      Revisore:
;xxxxxxx같
;xxxxxxx같   xxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx컓xxxxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같      Note:                                             xxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxxxx같



; Gestione sonda P3 : (PTC) Pt1000 & NTC

;  La taratura viene eseguita distintamente per (PTC) Pt1000 e NTC.

;_nu_;  I punti di taratura della sonda PTC sono :

;_nu_;       -50캜 = 509,7 Ohm           147캜 = 2163,2 Ohm

;  I punti di taratura della sonda PT1000 sono :

;       -90캜 = 643 Ohm           127캜 = 1487 Ohm

;  I punti di taratura della sonda NTC sono :

;       -40캜 = 188K5 Ohm            71캜 = 2163   Ohm



; Tabella dei punti di linearizzazione sonda PTC 

; Formula per calcolo valore temperatura :

;             NPretta * GAIN(ptc)
; VAL =     ---------------------- + OFFSET(ptc)
;                0x4000

; Pull-Up   : 10Kohm
; Pull-Down : 10Kohm
; Per sonda : P3 
; 100 acquisizioni

; Solo per sonda P3 (circuito d'ingresso con resistenza di Pull-Down)
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
TB_LNZ_P3:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
; Formula per calcolo valore temperatura :

;             NPretta * GAIN(ntc)
; VAL =     ---------------------- + OFFSET(ntc)
;                0x8000

; Pull-Up   : 10Kohm
; Pull-Down : 10Kohm
; Per sonda : P3 
; 100 acquisizioni

;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
;  P T C 
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

LNZ_PTC_P3_NEG:	equ	((* - TB_LNZ_P3) / 6)

LNZ_PTC_P3_POS:	equ	((* - TB_LNZ_P3) / 6)

;1� retta di linearizzazione		
	dc.w	4224
	dc.w	07CCh
	dc.w	0E9C2h
		
;2� retta di linearizzazione		
	dc.w	4478
	dc.w	07D8h
	dc.w	0E9AFh
		
;3� retta di linearizzazione		
	dc.w	4730
	dc.w	07F4h
	dc.w	0E97Eh
		
;4� retta di linearizzazione		
	dc.w	4945
	dc.w	0955h
	dc.w	0E6F2h
		
;5� retta di linearizzazione		
	dc.w	5164
	dc.w	0919h
	dc.w	0E765h
		
;6� retta di linearizzazione		
	dc.w	5389
	dc.w	08E3h
	dc.w	0E7D3h
		
;7� retta di linearizzazione		
	dc.w	5620
	dc.w	08A8h
	dc.w	0E84Fh
		
;8� retta di linearizzazione		
	dc.w	5855
	dc.w	0876h
	dc.w	0E8BDh
		
;9� retta di linearizzazione		
	dc.w	6097
	dc.w	0849h
	dc.w	0E924h
		
;10� retta di linearizzazione		
	dc.w	6342
	dc.w	082Bh
	dc.w	0E96Ch
		
;11� retta di linearizzazione		
	dc.w	6592
	dc.w	07FFh
	dc.w	0E9DAh
		
;12� retta di linearizzazione		
	dc.w	6845
	dc.w	07DEh
	dc.w	0EA2Dh
		
;13� retta di linearizzazione		
	dc.w	7105
	dc.w	07B7h
	dc.w	0EA95h
		
;14� retta di linearizzazione		
	dc.w	7367
	dc.w	079Eh
	dc.w	0EADBh
		
;15� retta di linearizzazione		
	dc.w	7633
	dc.w	077Bh
	dc.w	0EB41h
		
;16� retta di linearizzazione		
	dc.w	7903
	dc.w	076Ah
	dc.w	0EB75h
		
;17� retta di linearizzazione		
	dc.w	8175
	dc.w	0755h
	dc.w	0EBB4h
		
;18� retta di linearizzazione		
	dc.w	8452
	dc.w	073Bh
	dc.w	0EC06h
		
;19� retta di linearizzazione		
	dc.w	8732
	dc.w	0729h
	dc.w	0EC42h
		
;20� retta di linearizzazione		
	dc.w	9014
	dc.w	0711h
	dc.w	0EC95h
		
;21� retta di linearizzazione		
	dc.w	9299
	dc.w	06FFh
	dc.w	0ECD6h
		
;22� retta di linearizzazione		
	dc.w	9585
	dc.w	06FDh
	dc.w	0ECDCh
		
;23� retta di linearizzazione		
	dc.w	9873
	dc.w	06EBh
	dc.w	0ED20h
		
;24� retta di linearizzazione		
	dc.w	10166
	dc.w	06D9h
	dc.w	0ED66h
		
;25� retta di linearizzazione		
	dc.w	10459
	dc.w	06D1h
	dc.w	0ED84h
		
;26� retta di linearizzazione		
	dc.w	10755
	dc.w	06C2h
	dc.w	0EDC1h
		
;27� retta di linearizzazione		
	dc.w	11052
	dc.w	06BDh
	dc.w	0EDD8h
		
;28� retta di linearizzazione		
	dc.w	11351
	dc.w	06B1h
	dc.w	0EE0Ah
		
;29� retta di linearizzazione		
	dc.w	11649
	dc.w	06B1h
	dc.w	0EE0Ah
		
;30� retta di linearizzazione		
	dc.w	11950
	dc.w	06A6h
	dc.w	0EE3Dh
		
;31� retta di linearizzazione		
	dc.w	12251
	dc.w	06A6h
	dc.w	0EE3Dh
		
;32� retta di linearizzazione		
	dc.w	12554
	dc.w	069Fh
	dc.w	0EE5Fh
		
;33� retta di linearizzazione		
	dc.w	12857
	dc.w	0697h
	dc.w	0EE85h
		
;34� retta di linearizzazione		
	dc.w	13161
	dc.w	0694h
	dc.w	0EE96h
		
;35� retta di linearizzazione		
	dc.w	13460
	dc.w	06AEh
	dc.w	0EE0Dh
		
;36� retta di linearizzazione		
	dc.w	13763
	dc.w	0699h
	dc.w	0EE7Fh
		
;37� retta di linearizzazione		
	dc.w	14062
	dc.w	06B1h
	dc.w	0EDFAh
		
;38� retta di linearizzazione		
	dc.w	14350
	dc.w	06EDh
	dc.w	0ECB4h
		
;39� retta di linearizzazione		
	dc.w	14628
	dc.w	0733h
	dc.w	0EB2Bh
		
;40� retta di linearizzazione		
	dc.w	14888
	dc.w	07B6h
	dc.w	0E83Dh
		
;41� retta di linearizzazione		
	dc.w	15142
	dc.w	07DDh
	dc.w	0E75Bh
		
;42� retta di linearizzazione		
	dc.w	15359
	dc.w	0926h
	dc.w	0DFC1h
		
;43� retta di linearizzazione		
	dc.w	15574
	dc.w	094Dh
	dc.w	0DED9h


END_LNZ_PTC_P3:



; Solo per sonda P3 (circuito d'ingresso con resistenza di Pull-Down)
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
;TB_LNZ_P3:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
; Formula per calcolo valore temperatura :

;             NPretta * GAIN(ntc)
; VAL =     ---------------------- + OFFSET(ntc)
;                0x8000

; Pull-Up   : 10Kohm
; Pull-Down : 10Kohm
; Per sonda : P3 
; 100 acquisizioni

;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
;  N T C 
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

LNZ_NTC_P3_NEG:	equ	((* - TB_LNZ_P3) / 6)

;1� retta di linearizzazione		
	dc.w	50137
	dc.w	0F7F3h
	dc.w	096A8h
		
;2� retta di linearizzazione		
	dc.w	49828
	dc.w	0F98Ah
	dc.w	07794h
		
;3� retta di linearizzazione		
	dc.w	49433
	dc.w	0FAEBh
	dc.w	05CB0h
		
;4� retta di linearizzazione		
	dc.w	48950
	dc.w	0FBDCh
	dc.w	04A86h
		
;5� retta di linearizzazione		
	dc.w	48351
	dc.w	0FCA9h
	dc.w	03B3Ah
		
;6� retta di linearizzazione		
	dc.w	47634
	dc.w	0FD36h
	dc.w	030D2h
		
;7� retta di linearizzazione		
	dc.w	46769
	dc.w	0FDB1h
	dc.w	027E0h
		
;8� retta di linearizzazione		
	dc.w	45760
	dc.w	0FE05h
	dc.w	021E1h
		
;9� retta di linearizzazione		
	dc.w	44574
	dc.w	0FE51h
	dc.w	01C97h
		
;10� retta di linearizzazione		
	dc.w	43225
	dc.w	0FE85h
	dc.w	0190Ch
		
;11� retta di linearizzazione		
	dc.w	41693
	dc.w	0FEB2h
	dc.w	0160Ch
		
;12� retta di linearizzazione		
	dc.w	40009
	dc.w	0FED0h
	dc.w	01420h
		
;13� retta di linearizzazione		
	dc.w	38159
	dc.w	0FEECh
	dc.w	01272h
		
;14� retta di linearizzazione		
	dc.w	36183
	dc.w	0FEFEh
	dc.w	0116Ah
		
;15� retta di linearizzazione		
	dc.w	34098
	dc.w	0FF0Bh
	dc.w	010ADh

LNZ_NTC_P3_POS:	equ	((* - TB_LNZ_P3) / 6)
		
;16� retta di linearizzazione		
	dc.w	31938
	dc.w	0FF14h
	dc.w	0103Bh
		
;17� retta di linearizzazione		
	dc.w	29728
	dc.w	0FF19h
	dc.w	00FFBh
		
;18� retta di linearizzazione		
	dc.w	27526
	dc.w	0FF18h
	dc.w	01003h
		
;19� retta di linearizzazione		
	dc.w	25343
	dc.w	0FF16h
	dc.w	01018h
		
;20� retta di linearizzazione		
	dc.w	23228
	dc.w	0FF0Fh
	dc.w	01063h
		
;21� retta di linearizzazione		
	dc.w	21188
	dc.w	0FF06h
	dc.w	010B1h
		
;22� retta di linearizzazione		
	dc.w	19258
	dc.w	0FEF7h
	dc.w	0112Ch
		
;23� retta di linearizzazione		
	dc.w	17444
	dc.w	0FEE7h
	dc.w	011A7h
		
;24� retta di linearizzazione		
	dc.w	15766
	dc.w	0FED0h
	dc.w	01244h
		
;25� retta di linearizzazione		
	dc.w	14211
	dc.w	0FEB8h
	dc.w	012D7h
		
;26� retta di linearizzazione		
	dc.w	12793
	dc.w	0FE97h
	dc.w	0138Ch
		
;27� retta di linearizzazione		
	dc.w	11504
	dc.w	0FE74h
	dc.w	0143Dh
		
;28� retta di linearizzazione		
	dc.w	10334
	dc.w	0FE4Bh
	dc.w	014F3h
		
;29� retta di linearizzazione		
	dc.w	9278
	dc.w	0FE1Ch
	dc.w	015B1h
		
;30� retta di linearizzazione		
	dc.w	8331
	dc.w	0FDE3h
	dc.w	01681h
		
;31� retta di linearizzazione		
	dc.w	7484
	dc.w	0FDA4h
	dc.w	0174Eh
		
;32� retta di linearizzazione		
	dc.w	6729
	dc.w	0FD5Ch
	dc.w	01821h




; Formula per calcolo valore temperatura :

;             NPretta * GAIN(Pt1000)
; VAL =     ---------------------- + OFFSET(Pt1000)
;                0x8000

; Pull-Up   : 10Kohm
; Pull-Down : 10Kohm
; Per sonda : P3 
; 100 acquisizioni

;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
;  P T 1 0 0 0 
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

LNZ_PT1000_P3_NEG:	equ	((* - TB_LNZ_P3) / 6)

LNZ_PT1000_P3_POS:	equ	((* - TB_LNZ_P3) / 6)

;1� retta di linearizzazione		
	dc.w	5663
	dc.w	0C3Ch
	dc.w	0D618h
		
;2� retta di linearizzazione		
	dc.w	5827
	dc.w	0C37h
	dc.w	0D623h
		
;3� retta di linearizzazione		
	dc.w	5988
	dc.w	0C74h
	dc.w	0D596h
		
;4� retta di linearizzazione		
	dc.w	6148
	dc.w	0C8Eh
	dc.w	0D55Ah
		
;5� retta di linearizzazione		
	dc.w	6305
	dc.w	0CBCh
	dc.w	0D4E9h
		
;6� retta di linearizzazione		
	dc.w	6463
	dc.w	0CC9h
	dc.w	0D4C9h
		
;7� retta di linearizzazione		
	dc.w	6618
	dc.w	0CE8h
	dc.w	0D479h
		
;8� retta di linearizzazione		
	dc.w	6772
	dc.w	0D08h
	dc.w	0D429h
		
;9� retta di linearizzazione		
	dc.w	6925
	dc.w	0D1Dh
	dc.w	0D3EEh
		
;10� retta di linearizzazione		
	dc.w	7077
	dc.w	0D40h
	dc.w	0D390h
		
;11� retta di linearizzazione		
	dc.w	7226
	dc.w	0D59h
	dc.w	0D34Ah
		
;12� retta di linearizzazione		
	dc.w	7375
	dc.w	0D6Dh
	dc.w	0D310h
		
;13� retta di linearizzazione		
	dc.w	7523
	dc.w	0D98h
	dc.w	0D295h
		
;14� retta di linearizzazione		
	dc.w	7670
	dc.w	0DA4h
	dc.w	0D270h
		
;15� retta di linearizzazione		
	dc.w	7815
	dc.w	0DBFh
	dc.w	0D221h
		
;16� retta di linearizzazione		
	dc.w	7958
	dc.w	0E00h
	dc.w	0D158h
		
;17� retta di linearizzazione		
	dc.w	8102
	dc.w	0DF0h
	dc.w	0D18Ah
		
;18� retta di linearizzazione		
	dc.w	8243
	dc.w	0E35h
	dc.w	0D0B0h
		
;19� retta di linearizzazione		
	dc.w	8384
	dc.w	0E3Ah
	dc.w	0D0A0h
		
;20� retta di linearizzazione		
	dc.w	8523
	dc.w	0E67h
	dc.w	0D00Ch
		
;21� retta di linearizzazione		
	dc.w	8662
	dc.w	0E6Eh
	dc.w	0CFF3h
		
;22� retta di linearizzazione		
	dc.w	8799
	dc.w	0E98h
	dc.w	0CF65h
		
;23� retta di linearizzazione		
	dc.w	8934
	dc.w	0ECAh
	dc.w	0CEB8h
		
;24� retta di linearizzazione		
	dc.w	9069
	dc.w	0EDFh
	dc.w	0CE71h
		
;25� retta di linearizzazione		
	dc.w	9202
	dc.w	0F08h
	dc.w	0CDDDh
		
;26� retta di linearizzazione		
	dc.w	9337
	dc.w	0EEEh
	dc.w	0CE3Bh
		
;27� retta di linearizzazione		
	dc.w	9468
	dc.w	0F38h
	dc.w	0CD2Ch
		
;28� retta di linearizzazione		
	dc.w	9599
	dc.w	0F3Eh
	dc.w	0CD18h
		
;29� retta di linearizzazione		
	dc.w	9728
	dc.w	0F71h
	dc.w	0CC57h
		
;30� retta di linearizzazione		
	dc.w	9859
	dc.w	0F6Bh
	dc.w	0CC6Eh
		
;31� retta di linearizzazione		
	dc.w	9986
	dc.w	0FA7h
	dc.w	0CB86h
		
;32� retta di linearizzazione		
	dc.w	10113
	dc.w	0FB3h
	dc.w	0CB5Ah
		
;33� retta di linearizzazione		
	dc.w	10238
	dc.w	0FFBh
	dc.w	0CA3Dh
		
;34� retta di linearizzazione		
	dc.w	10365
	dc.w	0FF2h
	dc.w	0CA61h
		
;35� retta di linearizzazione		
	dc.w	10487
	dc.w	01058h
	dc.w	0C8C4h
		
;36� retta di linearizzazione		
	dc.w	10610
	dc.w	01045h
	dc.w	0C910h
		
;37� retta di linearizzazione		
	dc.w	10733
	dc.w	01042h
	dc.w	0C91Dh
		
;38� retta di linearizzazione		
	dc.w	10854
	dc.w	010A6h
	dc.w	0C77Ah
		
;39� retta di linearizzazione		
	dc.w	10974
	dc.w	010B6h
	dc.w	0C734h
		
;40� retta di linearizzazione		
	dc.w	11095
	dc.w	010B2h
	dc.w	0C747h
		
;41� retta di linearizzazione		
	dc.w	11212
	dc.w	010E4h
	dc.w	0C66Dh
		
;42� retta di linearizzazione		
	dc.w	11330
	dc.w	01100h
	dc.w	0C5F3h
		
;43� retta di linearizzazione		
	dc.w	11447
	dc.w	01139h
	dc.w	0C4F6h
		
;44� retta di linearizzazione		
	dc.w	11563
	dc.w	0112Eh
	dc.w	0C524h
		
;45� retta di linearizzazione		
	dc.w	11678
	dc.w	01163h
	dc.w	0C437h
		
;46� retta di linearizzazione		
	dc.w	11793
	dc.w	01169h
	dc.w	0C41Ah
		
;47� retta di linearizzazione		
	dc.w	11905
	dc.w	011CBh
	dc.w	0C25Ah
		
;48� retta di linearizzazione		
	dc.w	12018
	dc.w	011BFh
	dc.w	0C28Eh
		
;49� retta di linearizzazione		
	dc.w	12130
	dc.w	011F7h
	dc.w	0C186h
		
;50� retta di linearizzazione		
	dc.w	12241
	dc.w	011FBh
	dc.w	0C174h

END_LNZ_PT1000_P3:


END_TB_LNZ_P3:



