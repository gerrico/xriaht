

;------------------------------------------------------------------------------
; TABLEVER:     versione SW
;------------------------------------------------------------------------------
ANAG_CFP        equ     44	; Codice famiglia prodotto

ANAG_CVF        equ     243 ;232 ;202 ;105     ; 98  ;78      ; Codice versione FW 

ANAG_EEPROM     equ     219 ;209 ;181 ;94      ; 87  ;68      ; Codice versione EE


_DAY        	equ     10      ;04 ; 20 ;30 ;12 ;05 ;29 ;26 ;14 ;19 ;04 ;06(ZIP) ;31 ;04 ;24 ;28 ;07 ;01      ; 27        
_MONTH      	equ     12     ;11 ; 07 ;06 ;11 ;09 ;05 ;02 ;01 ;10 ;10 ;09(ZIP) ;07 ;06 ;05 ;02 ;04 ;07      ; 06                    
_YEAR       	equ     20     ;20 ; 20 ;20 ;19 ;19 ;19 ;19 ;19 ;18 ;18 ;18(ZIP) ;18 ;18 ;18 ;17 ;15 ;14      ; 14        	      

ANAG_DATA	equ	((_DAY << 11)+(_MONTH << 7)+_YEAR)	; Codice versione firmware
                        