
;xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같      Progetto :              serie "P R I M E"         xxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같   xxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx컓xxxxxxxxx같
;xxxxxxx같
;xxxxxxx같      Data Creazione :        26 - 11 - 2002
;xxxxxxx같
;xxxxxxx같   xxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx컓xxxxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같      Revisione:                                        xxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같   xxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx컓xxxxxxxxx같
;xxxxxxx같
;xxxxxxx같      Revisore:
;xxxxxxx같
;xxxxxxx같   xxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx컓xxxxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같      Note:                                             xxxxxxx같
;xxxxxxx같                                                        xxxxxxx같
;xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxx같xxxxxxxxx같

;        header 'DRIVER - READ WRITE FLASH PARAMETRI '
;BASE 10T                       ; numeri per default in decimale

;� Descrizione :
;  Per la memorizzazione dei parametri di funzionamento del dispositivo
;  viene impiegata la memoria FLASH del micro (KX8).   Questa memoria
;  contiene sia il programma che i parametri.   Tutta la memoria FLASH
;  � suddivisa in pagine di 64 byte ciascuna (2 righe di 32 byte).
;  Per la memorizzazione dei parametri vengono utilizzate 3 pagine FLASH.
;  1) La prima pagina contiene i parametri di funzionamento del dispositivo.
;     Questi parametri vengono scritti in fase di collaudo del dispositivo
;     ma possono essere modificati dall'utilizzatore in qualunque momento.
;  2) La seconda pagina contiene i parametri di funzionamento del dispositivo.
;     Questi parametri vengono scritti in fase di collaudo del dispositivo
;     ma possono essere modificati dall'utilizzatore in qualunque momento.
;  3) La terza pagina contiene i parametri di taratura delle sonde ed i
;     parametri di configurazione (di fabbrica) del dispositivo.   Questi
;     parametri vengono scritti in fase di collaudo del dispositivo e non
;     vengono pi� modificati.
;  Le prime due pagine (PAGE_0 e PAGE_1) dispongono di un contatore che indicano
;  quale pagina � stata l'ultima ad essere aggiornata e quindi contiene i
;  parametri da utilizzare (pagina MASTER).  Per l'aggiornamento di un
;  parametro � necessario azzerare prima di tutto la pagina al momento
;  non utilizzata (pagina SLAVE) mantenendo inalterata la pagina MASTER.
;  Quindi copiare l'intero blocco di parametri (disponibile in RAM), nella
;  pagina appena azzerata aumentando di due il valore del contatore (uno in
;  pi� del contatore della pagina MASTER).  Dopo la copia, la pagina appena
;  aggiornata diventa la pagina MASTER avendo il proprio contatore un valore
;  superiore all'altra pagina.

;  Se interviene un reset o un power failure, prima di aver potuto salvare
;  i dati aggiornati nella pagina, rimangono comunque validi
;  quelli dell'altra (MASTER).   Viene calcolato il CRC dei dati allo scopo
;  di controllare al power-on la correttezza dei dati nelle due pagine.
;  Inoltre vengono confrontate le due pagine (2� e 3�) che devono risultare
;  identiche in caso contrario se il crc della 3� pagina � corretto, viene
;  copiata la 3� pagina ("Copia") nella 2� pagina ("Principale").




; Comandi FLASH
 
FLS_CMD_BLANK_CHECK	equ	05H	
FLS_CMD_PROG		equ	20H	
FLS_CMD_PROG_BURST	equ	25H	
FLS_CMD_PAGE_ERASE	equ	40H	
FLS_CMD_MASS_ERASE	equ	41H	





; PowerOn control
; - Al power on controlla le due pagine parametri :
; Controlla che il "CONTATORE" di pagina sia congruente :
; 1) non deve essere 0xFF perch� quando si azzera la pagina tutta la FLASH �
;    a 0xFF e deve essere maggiore di 0.
; 2) deve essere maggiore di 1 rispetto a quello dell'altra pagina
; Se entrambi i "CONTATORI" sono 0xFF, viene selezionata la prima pagina.

;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_PO_Ctrl:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
;� Seleziona la pagina parametri tra la prima ("0") e la seconda ("1")
        bclr    F_PAGE.PAR,FLAG

        lda     (FPAGE0_STARTPAR+Idx_CNT_PAGE)
        cbeqa   #0ffh,FLS_POC.04

        lda     (FPAGE1_STARTPAR+Idx_CNT_PAGE)
        cbeqa   #0ffh,FLS_POC.10

        sub     (FPAGE0_STARTPAR+Idx_CNT_PAGE)
        bcs     FLS_POC.02
        cmp     #3
        bhi     FLS_POC.10
        bra     FLS_POC.06
FLS_POC.02:
        nega
        cmp     #3
        bls     FLS_POC.10
        bra     FLS_POC.06

FLS_POC.04:
        lda     (FPAGE1_STARTPAR+Idx_CNT_PAGE)
        cbeqa   #0ffh,FLS_POC.10

FLS_POC.06:
        bset    F_PAGE.PAR,FLAG

FLS_POC.10:

; Verifica il valore dei parametri.  Se uno o pi� parametri devono essere
;  modificati, attiva la procedura di WRITE della FLASH
; Copia i 32 byte (alti o bassi) dei parametri dalla pagina FLASH in RAM
; : Uty_Flash[bit7] = 0 FASE DI TEST DEI PARAMETRI
; : Uty_Flash[bit7] = 1 FASE DI SCRITTURA DEI PARAMETRI
        clr     Work9+0

FLS_POC.12:

FLS_POC.14:
        jsr     Rd_1B2B_PAR

; Copia tutti i parametri in RAM (RamParametri)
	jsr	FLASH_PageParamCopy
        
        jsr     Chk_Upd_Param

        brclr   0,Work9+0,FLS_POC.30   ; flag locale : aggiorna in FLASH

        jsr     FLASH_MakePg0Blank

 	jmp	FLASH_PageCopy

FLS_POC.CRC:

; Cambia pagina parametri
        lda     #(1 << F_PAGE.PAR)
        eor     FLAG
        sta     FLAG

FLS_POC.30:


;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
Rd_1B2B_PAR:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
; Carica parametri a 1bit e 2 bit

        jsr	load_MAPPA_PFAddress
        mov     x+,Copy_Par_1Bit+0
        mov     x+,Copy_Par_1Bit+1
        mov     x+,Copy_Par_1Bit+2
        mov     x+,Copy_Par_1Bit+3
        mov     x+,Copy_Par_1Bit+4
        mov     x+,Copy_Par_1Bit+5
        mov     x+,Copy_Par_2Bit+0	; Par. a 2 bit
        mov     x+,Copy_Par_2Bit+1	; Par. a 2 bit

        clrh
	rts



; Funzione chiamata ogni POWER-ON
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
Rd_SttDevice:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

; Cerca l'ultimo RECORD scritto 

	jsr	GetLastRecord

	sta	Fls_IdxRecord

	bcc	Rd_SttDev_10		; CY=0 OK    CY=1 NOK		

	ldhx	#TAB_STATI		; Valori di default

Rd_SttDev_10:

	clra				; Indice per Stt_Device0

Rd_SttDev_20:

	psha

        lda     ,x
	aix	#1
	pshh
	pshx

	clrh
	ldx	3,SP

	sta     Stt_Device0,x
        
	pulx
	pulh

	pula

	inca
	cmp     #10;#6
        
	blo     Rd_SttDev_20

        rts

TAB_STATI:
	dc.b	01h,00h
	dc.b	0ffh,0ffh
	dc.b	0ffh,0ffh
	dc.b  0ffh,0ffh


;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
Chk_Upd_Param:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
; Controlla all'accensione la congruenza del valore dei parametri

        bset    F_CHKPAR,gp1_flag
        
	bclr   F_MAPPA,Stt_Device0
	
	jsr     Rd_1B2B_PAR
	
CPR_0:	
; !!! ATTENZIONE che non venga sporcato Work9+0 !!!!!

        clr     PUNT_PAR
CPR_1:
        sta     SRS		; reset watch-dog micro

        jsr     LOAD_PAR        ; carica valore parametro da EEPROM
        sthx    PAR
        clrh
        jsr     CHK_PAR
        bcc     CPR_20

        jsr     SAVE_PAR_PO
        bset    0,Work9+0       ; flag locale : aggiorna in FLASH

CPR_20:
        inc     PUNT_PAR
        lda     PUNT_PAR
        cmp	#Y_HUR
        blo	CPR_1
        cmp	#Y_YAR  
        bls	CPR_20
        
;_no_   cmp     #Y_LASTPAR      ; ultimo parametro?
        cmp     #Y_SET          ; ultimo parametro?
        bls     CPR_1
        
	brset   F_MAPPA,Stt_Device0,CPR_25

	bset	F_MAPPA,Stt_Device0
	
	jsr     Rd_1B2B_PAR
	
	bra	CPR_0

CPR_25:
        bclr    F_CHKPAR,gp1_flag

        rts


; Cancellazione di una delle tre pagine FLASH seguenti :
;    - FLASH_PAGE0 , FLASH_PAGE1 , FLASH_PAGE2
; In accumulatore viene passato un parametro che se vale :
;    0 : viene azzerata o FLASH_PAGE0 o FLASH_PAGE1 in base a F_PAGE.PAR
;    1 : viene azzerata FLASH_PAGE2
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_MakePg0Blank:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx

	ldhx	#FLASH_PAGE0
	brset   F_PAGE.PAR,FLAG,FLASH_MakePgBlank
 	ldhx	#FLASH_PAGE1

;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_MakePgBlank:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        clr     Work8+0                 ; numero tentativi
FLASH_MPB.2:

	pshh
	pshx
        bsr     FLASH_PgChkBlk
	pulx
	pulh

        beq     FLASH_MPB.9

        inc     Work8+0                 ; numero tentativi
        brset   3,Work8+0,FLASH_MPB.8

	pshh
	pshx

	sthx	Work1
        
;	bset	2,PORT_E

; Abortisce l'acquisizione analogica per alterazione 
; del segnale rumore. 
	bset	F_AD_FLASH,FlgAD  

	sei
	jsr     FLASH_ERASE_Driver    ; Funzione interna al micro
        cli
        
;	bclr	2,PORT_E


	pulx
	pulh

        bra     FLASH_MPB.2

FLASH_MPB.8:
        bset	A2.EE,StatoAllarmi+1

FLASH_MPB.9:
	clrh
	rts        



;� Verifica la cancellazione della pagina FLASH non utilizzata
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_PgChkBlk:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx

	lda	#((WSIZE_ALL_PAR * 2) + 2)
	psha

FLS_PCB.1:

	lda     ,x

FLS_PCB.2:
        inca                    ; deve essere : FF
        bne     FLS_PCB.3
        
	aix	#1

	dbnz	1,SP,FLS_PCB.1

FLS_PCB.3:
	pula
        rts





;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_ProgSttDev:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx

	;mov	#8,Work3+0
	ldhx  #12;#8
	sthx  Work3
	
	lda	#0FFH
	sta	RamParametri+10;6

	lda	Fls_IdxRecord
	sta	RamParametri+11;7

	jsr	GetAddrRecord

;_ns_	sthx	Work2
;_ns_	ldhx	#RamParametri		; FLASH_PAGE0
;_ns_	sthx	Work1
;_ns_	jmp     FLASH_WRITE_Driver    	; Funzione interna al micro

	jmp     FLASH_WRITE	    	; Funzione interna al micro




;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_PageCopy:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx

	bsr	FLASH_SetCrc
  clc
; Dimensione blocco parametri + 2 byte di CRC & NumPAGE 
	;mov	#((WSIZE_ALL_PAR * 2)+2),Work3+0
  ldhx  #((WSIZE_ALL_PAR*2)+2)
  sthx  Work3
  
  ldhx	#FLASH_PAGE0 
	brset   F_PAGE.PAR,FLAG,FLASH_PC_10
 	ldhx	#FLASH_PAGE1 

FLASH_PC_10:

;	sthx	Work2
;	ldhx	#RamParametri		; FLASH_PAGE0
;	sthx	Work1
;      	jsr     FLASH_WRITE_Driver    	; Funzione interna al micro

	jsr     FLASH_WRITE	    	; Funzione interna al micro
        
        jmp	FLS_POC.CRC




; Aggiorna CRC e CONTATORE PAGINA
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_SetCrc:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx

; Calcola CRC dei DATI 
        lda     #(WSIZE_ALL_PAR * 2)
	psha

	ldhx	#RamParametri
        
FLS_Crc_2:
        clra

FLS_Crc_4:
        add     ,x
        aix	#1
        dbnz    1,SP,FLS_Crc_4

        sta     (RamParametri + Idx_CRC_PAGE)
	pula


        lda     (FLASH_PAGE0 + Idx_CNT_PAGE)
        brclr   F_PAGE.PAR,FLAG,FLS_PgCrc.2
        lda     (FLASH_PAGE1 + Idx_CNT_PAGE)

FLS_PgCrc.2:

        inca
        cmp     #0ffh
        blo     FLS_PgCrc.4
        inca

FLS_PgCrc.4:

        sta     RamParametri+Idx_CNT_PAGE
	rts





; In HX viene passato l'indirizzo della pagina FLAH da cancellare
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_ERASE_Driver:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx

; Copia la funzione di scrittura in RAM (Stack)

	tsx
	sthx	Work5			; salva indirizzo stack	

	mov	#(OnStk_FlsBlank_End-OnStk_FlsBlank),Work4+0		; Size funzione	

	ldhx	#(OnStk_FlsBlank_End-1)	; Funzione di scrittura FLASH

	bra	CopyOnStack	 



;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_WRITE:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx

; Abortisce l'acquisizione analogica per alterazione 
; del segnale rumore. 
	bset	F_AD_FLASH,FlgAD  


	sthx	Work2

	ldhx	#RamParametri		; FLASH_PAGE0
	sthx	Work1

; In Work1[] viene passato l'indirizzo SORGENTE (dati da copiare) 
; In Work2[] viene passato l'indirizzo DESTINAZIONE 
; I byte da copiare sono stabiliti dal buffer FLS_CopyEnable
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
;FLASH_WRITE_Driver:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
; Copia la funzione di scrittura in RAM (Stack)

	tsx
	sthx	Work5			; salva indirizzo stack	

	mov	#(OnStk_FlsWrite_End-OnStk_FlsWrite),Work4+0		; Size funzione	

	ldhx	#(OnStk_FlsWrite_End-1)	; Funzione di scrittura FLASH

CopyOnStack:

	lda	,x
	psha
	aix	#-1
	dbnz	Work4+0,CopyOnStack

	bra	Back_CopyOnStack_WR_ERS	; Indirizzo di rientro
	

; Rientro comune sia per ERASE che WRITE

Back_CopyOnStack_WR_ERS:

	tsx			; XH = indirizzo funzione 

; 1) Se errore FACCERR/FPVIOL = 1 , deve resettarlo

        lda     #(mFSTAT_FPVIOL | mFSTAT_FACCERR) 
        sta     FSTAT

	sei
	jsr	,x		; Chiama la funzione

	ldhx	Work5
	txs

;_no_	ais	#(OnStk_FlsWrite_End-OnStk_FlsWrite)

FLS_DrvPrg_30:

	cli
	bcs     FLS_DrvPrg_Err

; 9) Check se operazione completata

FLS_DrvPrg_40:

	clc		; OK
	rts

FLS_DrvPrg_Err:

	sec		; Errore 
	rts



;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
OnStk_FlsBlank:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx

; In HX c'� l'indirizzo della pagina FLASH da cancellare

	ldhx	Work1
        sta	,x
	
; 4) Imposta il comando in FCMD

	lda	#FLS_CMD_PAGE_ERASE	
	sta	FCMD

; 5) Attiva il comando : setta flag FCBEF
 
        lda     #mFSTAT_FCBEF	 
	sta     FSTAT
        

; 6) Aspetta almeno 4 cicli macchina 

	mul				; 5 cicli

; 7) Check errori : FPVIOL & FACCERR

        lda     #(mFSTAT_FPVIOL | mFSTAT_FACCERR) 
	bit     FSTAT
        bne	OnStk_FlsBlk_Err


; 9) Check se operazione completata

 OnStk_FlsBlk_40:

        lda     #mFSTAT_FCCF 
	bit     FSTAT
        beq	OnStk_FlsBlk_40

	clc		; OK
	rts

OnStk_FlsBlk_Err:

	sec		; Errore 
	rts
OnStk_FlsBlank_End:



;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
OnStk_FlsWrite:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx

	ldhx	Work1
	lda	,x
 
;_??_	cbeqa	#0FFH,OnStk_FlsWr_30

	ldhx	Work2

; 2) Se Buffer Comando impegnato , aspetta

	psha

OnStk_FlsWr_13:

        lda     #mFSTAT_FCBEF	; Buffer command empty ? 
	bit     FSTAT
        beq     OnStk_FlsWr_13

; 3) Scrive in FLASH il byte : scansione del buffer FLS_CopyEnable

	pula
	sta	,x

; 4) Imposta il comando in FCMD

	lda	#FLS_CMD_PROG_BURST	
	sta	FCMD

; 5) Attiva il comando : setta flag FCBEF
 
	lda     #mFSTAT_FCBEF	 
	sta     FSTAT
  
; 6) Aspetta almeno 4 cicli macchina 
	
	nop
	nop

; 7) Check errori : FPVIOL & FACCERR

        lda     #(mFSTAT_FPVIOL | mFSTAT_FACCERR) 
	bit     FSTAT
        bne	OnStk_FlsWr_Err

; 8) Check se altro da scrivere

OnStk_FlsWr_30:
	ldhx  Work1
	aix   #1
	sthx  Work1
	ldhx  Work2
	aix   #1
	sthx  Work2

  ldhx  Work3
  aix   #-1
  cphx  #0
  beq   OnStk_FlsWr_40
  
  sthx  Work3
  bra   OnStk_FlsWrite

OnStk_FlsWr_40:

        lda     #mFSTAT_FCCF 
	bit     FSTAT
        beq	OnStk_FlsWr_40

	clc
	rts

OnStk_FlsWr_Err:

	sec
	rts

OnStk_FlsWrite_End:




;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_PageStatusCopy:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx

	ldhx	#10 ; 10 byte ;#6		; 6 byte
	sthx  Work1

	ldhx	#Stt_Device0

	bra	FLASH_RamPageSet

;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_PageParamCopy:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx

	ldhx	#(WSIZE_ALL_PAR * 2)
	sthx  Work1

	ldhx	#FLASH_PAGE0 
	brclr   F_PAGE.PAR,FLAG,FLASH_RamPageSet
 	ldhx	#FLASH_PAGE1 

; Copia i parametri della pagina FLASH attiva in Ram
; Attenzione : utilizza l'indirizzo basso (X) sia per il Sorgente
; che per il Destinatario
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_RamPageSet:	
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
	
	
        clra			; Spiazzamento per RamParametri
        sta Work2+0
        sta Work2+1
        
        sthx  Work3
	
FLS_RPS_10:
	ldhx  Work3
	
	lda	,x		; Legge Dato in FLASH_PAGEx
	
	aix	#1
	sthx  Work3
	

	ldhx	Work2
	sta	RamParametri,x	
	aix   #1
	sthx	Work2


  ldhx	Work1
  aix   #-1
  beq   FLS_RPS_20
  
  sthx  Work1
  
  bra FLS_RPS_10
        
FLS_RPS_20:
	clrh
	rts







; Funzione di ricerca del RECORD-DATI degli stati funzionali
; del dispositivo tra gli 85 possibili (512 : 8 = 64).

; Struttura RECORD
;                   BYTE HIGH             BYTE LOW

; 1 word :      STATO FUNZIONALE        (NON UTILIZZATO)
; 2 word :      T E M P E R A T U R A   M I N I M A 
; 3 word :      T E M P E R A T U R A   M A S S I M A
; 4 word :         CRC                      INDICE              

;xxxxxxxxxxxxxxxxxxxxxxxxxxxx
GetLastRecord:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxx

	mov	#41,Work1+0;mov	#63,Work1+0	; indice ultimo ecord 

	ldhx	#(FLASH_PAGE2+503);ldhx	#(FLASH_PAGE2+511) ; Ultima locazione pagina

GetLR_05:

        lda	,x
	cmp	Work1+0
	beq	GetLR_20	; Trovato
        
	inca
	bne	GetLR_30	; Errore : RECORD NON TROVATO
	
	aix	#-12;#-8

	dbnz	Work1+0,GetLR_05

GetLR_20:

; 1� RECORD
	aix	#-11;#-7

	lda	Work1+0		; Indice RECORD
	clc
	rts

; Nessun RECORD inizializzato 
; Errore indice del RECORD  
GetLR_30:
	ldhx	#FLASH_PAGE2
	jsr     FLASH_MakePgBlank

	ldhx	#FLASH_PAGE2

	clra			; Indice RECORD
	sec
	rts

;_??_; Controllo RECORD
;_??_GetLR_40:
;_??_	aix	#-7
;_??_	lda	,x
;_??_	eor	1,x
;_??_	eor	2,x
;_??_	eor	3,x
;_??_	eor	4,x
;_??_	eor	5,x
;_??_	eor	6,x		; CRC = CRC
;_??_	bne	GetLR_30
;_??_
;_??_	clc
;_??_	rts	



; Ogni volta che si mette in LOCK/UNLOCK la tastiera, si controlla
; il registro FLASH-PROTECT. Se � 0xFF allora viene scritto con il 
; valore 0C4H.

;	brclr	F_COLLAUDO,FLAG,FLS_Set_NVP_10

;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_Prog_NVPROT:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
;;;;	lda	#0C4H
	lda	#084H
	sta	RamParametri+0

;;;;	mov	#1,Work3+0
        ldhx    #1
        sthx    Work3+0
	
	ldhx	#NVPROT	
	
	bra	FLS_WT_20


;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
FLASH_Prog_Taratura:
;xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                    
	;ldhx	#(32 + 2)
	ldhx	#(48 + 2) ; 16 * 3 (PTC-NTC-PT1000), 16 perch� sono 4 byte (2 gain e 2 offset) per 4 sonde
	sthx	Work3+0

FLS_WT_10:
	lda	COL_Taratura-1,x
	sta	RamParametri-1,x		
	dbnzx	FLS_WT_10

	ldhx	#T_A_R_A_T_U_R_A

FLS_WT_20:

;_ns_	sthx	Work2
;_ns_	ldhx	#RamParametri		
;_ns_	sthx	Work1
;_ns_	jmp     FLASH_WRITE_Driver    	; Funzione interna al micro

	jmp	FLASH_WRITE	